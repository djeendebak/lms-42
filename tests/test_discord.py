import re
import os
import random
import pytest
from lms42.app import app, db
from lms42.models.user import User
from flask_login import current_user

DISCORD_FILE_PATH = "/tmp/lms42-discord.txt"

@pytest.fixture(autouse=True)
def check_tests():
    random.seed(124) #sets the seed to 124 so that the same random numbers are generated every time (makes sure there wont be any needs_consent problems))

    if os.path.exists(DISCORD_FILE_PATH):
        os.remove(DISCORD_FILE_PATH)
    yield 
    if os.path.exists(DISCORD_FILE_PATH):
        os.remove(DISCORD_FILE_PATH)

def set_discord_id(user_short_name, discord_id):
    """Sets the discord id of a user with a given short name."""
    user = User.query.filter_by(short_name=user_short_name).first()
    user.discord_id = discord_id
    db.session.commit()


def start_exercise(client, exercise_name):
    """Starts an exercise with a given exercise name."""
    client.login('student1')
    client.open(f'/curriculum/{exercise_name}')
    client.find("input", value='Start attempt anyway').submit()
    client.find("input", value='Submit attempt').submit(finished="yes")
    client.find("input", value="Yes, I'm sure").submit()

def start_exam(client, exam_name):
    """Starts an exam with a given exam name. Also approves the exam by a teacher"""
    client.login('student1')
    client.open(f'/curriculum/{exam_name}')
    client.find("input.button", value="Start attempt anyway").submit()

    client.login('teacher1')
    client.open(f'/curriculum/{exam_name}?student=student1')
    client.find("input.button", value="Approve").submit()

    client.login('student1')
    client.open(f'/curriculum/{exam_name}')
    client.find("input", value='Submit attempt').submit(finished="yes")
    client.find("input", value="Yes, I'm sure").submit()

    
def grade_attempt(client, node_id, student_short_name, action, exam):
    """Somewhat copied from the grade_attempt function in test_pair_programming.py grade the attempt based on whether it's an exercise or exam."""
    client.login('teacher1')
    client.open(f'/curriculum/{node_id}?student={student_short_name}')
    if exam:
        form_data = {}
    else:
        form_data = {
            "formative_action": action
        }
    for element in client.find("input", type="radio", value="3"):
        form_data[element.get('name')] = "3" if action=="passed" else "1"
    for element in client.find("input", type="radio", value="yes"):
        form_data[element.get('name')] = "yes" if action=="passed" else "no"
    
    client.find("input", value="Publish", limit=1).submit(**form_data)

def test_exercise_failed(client, _db):
    start_exercise(client, "vars")
    grade_attempt(client, 'vars', 'student1', "failed", False)
    assert not os.path.isfile(DISCORD_FILE_PATH) # Checks that the file does not exist

def test_exercise_passed(client):
    start_exercise(client, "html")
    grade_attempt(client, 'html', 'student1', "passed", False)
    assert not os.path.isfile(DISCORD_FILE_PATH) # Checks that the file does not exist


def test_exam_passed(client):
    start_exam(client, 'pwa-project')
    set_discord_id('student1', 1)
    grade_attempt(client, 'pwa-project', 'student1', "passed", True)
    assert os.path.isfile(DISCORD_FILE_PATH) # Checks that the file does not exist
    with open(DISCORD_FILE_PATH, 'r') as file:
        content = file.read()
        assert "passed" in content # Checks whether the file contains the correct message


def test_exam_failed(client):
    start_exam(client, 'android-project')
    set_discord_id('student1', 1)
    grade_attempt(client, 'android-project', 'student1', "failed", True)
    assert os.path.isfile(DISCORD_FILE_PATH) # Checks that the file does not exist
    with open(DISCORD_FILE_PATH, 'r') as file:
        content = file.read()
        assert "failed" in content # Checks whether the file contains the correct message
