from ..app import db, app, scheduler, retry_commit
import flask
from .. import utils
from ..models.attempt import Attempt
from ..models.grading import Grading
from flask_login import login_required, current_user
from . import discord
from datetime import datetime, timedelta
import os
import shutil


@retry_commit
def check_recording() -> None:
    """Checks if the recording is still in progress. By checking the last screenshot time is more than 65 seconds ago.
    The attempt record status will be set to error and the student and all teachers will be notified."""

    attempts = Attempt.query \
        .filter_by(status="in_progress") \
        .filter_by(record_status="in_progress") \
        .filter(datetime.now() - Attempt.last_screenshot_time > timedelta(seconds=65)) \
        .all()

    for attempt in attempts:
                
        attempt.record_status = "error"
        db.session.commit()
        
        discord.message_teachers("Recording error", f"{attempt.student.first_name} {attempt.student.last_name} has stopped recording")
        
        if attempt.student.discord_id is not None:
            discord.send_dm(attempt.student.discord_id, "Recording error", "Screen recording has stopped! Please restart it as soon as possible! (Unless you have handed in your laptop to a teacher.)")
  
scheduler.add_job(check_recording, 'interval', seconds=60, id='check_recordings')


@retry_commit
def save_screenshot(image_data: bytes, attempt: Attempt) -> None:
    """Save screenshot in attempt.
    Sets the last screenshot time to datetime.now().
    Increases the max screenshot id by one.

    Args:
        image_data (bytes): The image data to save
        attempt (Attempt): The attempt to save the screenshot in
    """
    screenshot_location = os.path.join(attempt.directory, "screenshots")
    if attempt.last_screenshot_time is None:
        os.makedirs(screenshot_location, exist_ok=True)
            
    attempt.max_screenshot_id += 1
            
    with open(os.path.join(screenshot_location, f"{attempt.max_screenshot_id}.png"), 'wb') as file:
        file.write(image_data)
        
    attempt.last_screenshot_time = datetime.now()
    db.session.commit()


@retry_commit
def remove_expired_screenshots() -> None:
    """Queries all attempts that have been graded in the last 31 days and have been submitted 14 or more days ago.
    Removes the screenshots and sets the record_status to removed and set max_screenshot_id to 0."""""

    attempts = Attempt.query \
        .filter_by(record_status="finished") \
        .join(Grading, Attempt.id == Grading.attempt_id) \
        .filter(datetime.now() - Grading.time < timedelta(days=31)) \
        .filter(datetime.now() - Attempt.submit_time >= timedelta(days=14)) \
        .all()

    for attempt in attempts:
        screenshot_location = os.path.join(attempt.directory, "screenshots")
        shutil.rmtree(screenshot_location, ignore_errors=True)
        
        attempt.record_status = 'removed'
        attempt.max_screenshot_id = 0
        db.session.commit()
    
scheduler.add_job(remove_expired_screenshots,
    trigger='cron',
    day_of_week='mon-sun',
    hour=22,
    minute=0,
    id='remove_expired_screenshots'
)


@app.route('/record/upload', methods=['POST'])
@login_required
def upload_screenshot():
    attempt = current_user.current_attempt
    if attempt is None:
        return flask.jsonify({"status": "stop", "message": "No attempt found"})

    if attempt.record_status == "error":
        save_screenshot(flask.request.get_data(), attempt)
        discord.message_teachers("Recording resumed", f"{attempt.student.first_name} {attempt.student.last_name} has resumed recording")
        attempt.record_status = "in_progress"
        db.session.commit()
        return flask.jsonify({"status": "ok", "message": "Screenshot saved"})
    
    if attempt.status == "awaiting_recording":
        save_screenshot(flask.request.get_data(), attempt)
        attempt.status = "awaiting_approval"
        attempt.record_status = "in_progress"
        db.session.commit()
        return flask.jsonify({"status": "ok", "message": "Screenshot saved"})
    
    if attempt.record_status == "in_progress":
        save_screenshot(flask.request.get_data(), attempt)
        return flask.jsonify({"status": "ok", "message": "Screenshot saved"})
    
    if attempt.record_status == "finished":
        return flask.jsonify({"status": "stop", "message": "Recording stopped"})
    
    return flask.jsonify({"status": "stop", "message": "Can't start recording for this assignment"})
    

@app.route('/record/<int:attempt_id>/screenshot_count', methods=['GET'])
@utils.role_required('teacher')
def screenshot_list(attempt_id: int):
    attempt = Attempt.query.get(attempt_id)
    assert attempt
    return flask.jsonify({"max_id": attempt.max_screenshot_id})


@app.route('/record/<int:attempt_id>/screenshot/<int:screenshot_id>', methods=['GET'])
@utils.role_required('teacher')
def screenshot(attempt_id: int, screenshot_id: int):
    attempt = Attempt.query.get(attempt_id)
    screenshot_location_get = os.path.join(attempt.directory, "screenshots")
    
    create_time = os.path.getctime(os.path.join(screenshot_location_get, f"{screenshot_id}.png"))
    create_time_format = datetime.fromtimestamp(create_time).strftime('%Y-%m-%d %H:%M:%S')

    response = flask.make_response(flask.send_file(os.path.join('../', screenshot_location_get, f"{screenshot_id}.png")))  
    response.headers.extend({'File-Created': create_time_format})

    return response
