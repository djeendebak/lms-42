from ..app import app
from ..routes.inbox import get_students
import flask

@app.route('/tv', methods=['GET'])
@app.route('/tv/<device>', methods=['GET'])
def tv(device=None):
    fragment = flask.request.args.get('fragment')
    template = fragment if fragment in ('queue',) else 'tv'

    if device in ('a','b',):
        class_filter = f"ESD1V.{device}"
    elif device == 'd':
        class_filter = f"DSD1V.a"
    else:
        class_filter = None

    students = get_students(class_filter=class_filter)
    
    return flask.render_template(template+'.html',
        header=False,
        device=device,
        students=students
    )
