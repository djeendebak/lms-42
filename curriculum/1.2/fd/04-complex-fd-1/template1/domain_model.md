# Domain model

```plantuml
' Configuration stuff (you should not change this)
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho


entity "User (external)" as User {
  *name
}

entity "Game (external)" as Game {
  *name
  *description
  *image
}

note left of User
  These external entities already exist
  as part of the website. You can just
  use them in your ERD.
end note

User "owns" }o--o{ Game


```