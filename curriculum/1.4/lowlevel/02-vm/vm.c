// Build using:
// gcc -g -O3 vm.c -o vm

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>

typedef uint16_t PTR_TYPE;
const int PTR_SIZE = sizeof(PTR_TYPE);

uint8_t memory[64 * 1024];

const char *instrs[] = {"INVALID", "set", "add", "sub", "mul", "div", "and", "or", "xor", "if=", "if>", "if<", "read", "write"};

uint32_t load(uint32_t addr, int size) {
	if (size==1) {
    	return *(uint8_t *)(memory + addr);
	} else if (size==2) {
    	return *(uint16_t *)(memory + addr);
	} else {
    	return *(uint32_t *)(memory + addr);
	}
}

void store(uint32_t addr, uint32_t value, int size) {
	if (size==1) {
    	*(uint8_t *)(memory + addr) = (uint8_t)value;
	} else if (size==2) {
    	*(uint16_t *)(memory + addr) = (uint16_t)value;
	} else {
    	*(uint32_t *)(memory + addr) = (uint32_t)value;
	}
}

int main(int argc, char *argv[]) {
	uint8_t debug = 0;
	int argPos = 1;
	if (argc>=2 && !strcmp(argv[1],"-d")) {
		debug = 1;
		argPos++;
	}
	if (argc != argPos+1) {
		fprintf(stderr, "Syntax: %s [-d] <program_file>\n", argv[0]);
		return 1;
	}

	int fd = open(argv[argPos], O_RDONLY);
	if (fd<0) {
		fprintf(stderr, "Couldn't open program file: %s\n", argv[argPos]);
		return 2;
	}
	read(fd, memory, sizeof(memory));
	close(fd);

	uint8_t skip = 0;
	PTR_TYPE *ipPtr = (PTR_TYPE *)&memory[0];

	while(1) {
		uint32_t startIp = *ipPtr;
		if (*ipPtr==0) break; // halt

		uint8_t opcode = load((*ipPtr)++, 1);
		uint8_t argSpec = load((*ipPtr)++, 1);

		uint8_t wordSize = argSpec&3;
		if (wordSize==0) wordSize = 1;
		else if (wordSize==1) wordSize = 2;
		else if (wordSize==2) wordSize = 4;
		argSpec >>= 2;

	    void *argPtrs[2];
		uint32_t args[2];

		for(int argNum=0; argNum<2; argNum++) {
			uint8_t argType = argSpec&3;
			argSpec >>= 2;
			switch (argType) {
			case 0:
				args[argNum] = 0;
				argPtrs[argNum] = &args[argNum];
				break;
			case 1:
				argPtrs[argNum] = memory + *ipPtr;
				*ipPtr += wordSize;
				break;
			case 2:
				argPtrs[argNum] = memory + load(*ipPtr, PTR_SIZE);
				*ipPtr += PTR_SIZE;
				break;
			case 3:
				argPtrs[argNum] = memory + load(load(*ipPtr, PTR_SIZE), PTR_SIZE);
				*ipPtr += PTR_SIZE;
				break;
			}
			
			switch(wordSize) {
			case 1:
				args[argNum] = *(uint8_t *)argPtrs[argNum];
				break;
			case 2:
				args[argNum] = *(uint16_t *)argPtrs[argNum];
				break;
			case 4:
				args[argNum] = *(uint32_t *)argPtrs[argNum];
				break;
			}
		}

		if (skip) {
			skip = 0;
			continue;
		}

        if (debug && opcode < sizeof(instrs)/sizeof(instrs[0])) {
			fprintf(stderr, "%04x: %s%d %x(*%x) %x(*%x)\n", startIp, instrs[opcode], wordSize, args[0], argPtrs[0]==&args[0] ? 0 : (uint8_t *)argPtrs[0]-memory, args[1], argPtrs[1]==&args[1] ? 0 : (uint8_t *)argPtrs[1]-memory);
        }

		switch(opcode) {
		case 1:
			args[0] = args[1];
			break;
		case 2:
			args[0] += args[1];
			break;
		case 3:
			args[0] -= args[1];
			break;
		case 4:
			args[0] *= args[1];
			break;
		case 5:
			args[0] /= args[1];
			break;
		case 6:
			args[0] &= args[1];
			break;
		case 7:
			args[0] |= args[1];
			break;
		case 8:
			args[0] ^= args[1];
			break;
		case 9:
			if (!(args[0] == args[1])) skip = 1;
			break;
		case 10:
			if (!(args[0] > args[1])) skip = 1;
			break;
		case 11:
			if (!(args[0] < args[1])) skip = 1;
			break;
		case 12: {
			int ch = fgetc(stdin);
			args[0] = ch==EOF ? 256 : ch;
			break;
		}
		case 13:
			fputc(args[0]&255, stdout);
			break;
		default:
			fprintf(stderr, "Invalid opcode %x at %d\n", (int)opcode, startIp);
			return 1;
		}

		switch(wordSize) {
		case 1:
			*(uint8_t *)argPtrs[0] = args[0];
			break;
		case 2:
			*(uint16_t *)argPtrs[0] = args[0];
			break;
		case 4:
			*(uint32_t *)argPtrs[0] = args[0];
			break;
		}
		//fprintf(stderr, "%x %x %x %x ... %x %x\n", (int)memory[0], (int)memory[1], (int)memory[2], (int)memory[3], (int)memory[0x5b], (int)memory[0x5c]);
	}
		
	return 0;
}
