import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

abstract public class BusyBank {
    static final int ACCOUNTS = 1000; // number of bank users in the system
    static final int TRANSACTIONS = 100_000_000; // number of transactions to process
    static final int MAX_TRANSFER = 10_000_000; // the maximum amount in a single transaction


    volatile long[] balances = new long[ACCOUNTS];


    public static void main(String[] args) throws InterruptedException {
        while(true) {
            // Select a BusyBank
            BusyBank bank;
            System.out.print("\n1) sequential\n2) contended\n3) concurrent\nYour choice: ");
            Scanner scanner = new Scanner(System.in);
            String method = scanner.nextLine();
            if (method.equals("1")) bank = new SequentialBank();
            else if (method.equals("2")) bank = new ContendedBank();
            else if (method.equals("3")) bank = new ConcurrentBank();
            else continue;

            // Run the transactions
            long before = System.currentTimeMillis();
            bank.runTransactions();
            long after = System.currentTimeMillis();
            System.out.println("Execution time: " + (after - before) + "ms");

            // Check the resulting balances
            bank.checkBalances();
        }
    }


    class Transaction {
        int from;
        int to;
        long amount;

        public Transaction(int from, int to, long amount) {
            this.from = from;
            this.to = to;
            this.amount = amount;
        }
    }


    /**
     * This method returns the details for one of the transactions. Instead of reading the transaction from a large data
     * file, we're just (deterministically) making a random one up.
     * @param num The number of the transaction (0...TRANSACTIONS) we're insterested in.
     * @return The transaction details.
     */
    Transaction getTransaction(int num) {
        if (num<0 || num>=TRANSACTIONS) {
            throw new RuntimeException("Invalid transaction number "+num);
        }
        Random rand = new Random(num);
        return new Transaction(rand.nextInt(ACCOUNTS), rand.nextInt(ACCOUNTS), rand.nextInt(MAX_TRANSFER));
    }


    /**
     * Check to see the current balances reflect all transactions having been properly applied. Throws otherwise.
     */
    void checkBalances() {
        long sum = 0;
        long absSum = 0;
        for(int i=0; i<ACCOUNTS; i++) {
            sum += balances[i];
            absSum += Math.abs(balances[i]);
        }
        if (sum != 0) {
            throw new RuntimeException("Invalid account balance sum "+sum);
        }
        if (absSum != 621692654006L) {
            throw new RuntimeException("Invalid absolute account total "+absSum);
        }
    }


    /**
     * Obtain each of the transactions using getTransaction, and apply them to the balances. This abstract method should
     * be implemented for each of the different threading requirements.
     */
    abstract void runTransactions() throws InterruptedException;


    /**
     * This bank just applies all transactions sequentially, in a single thread.
     */
    static class SequentialBank extends BusyBank {
        @Override
        void runTransactions() {
            for(int i=0; i<TRANSACTIONS; i++) {
                Transaction tr = getTransaction(i);
                balances[tr.from] -= tr.amount;
                balances[tr.to] += tr.amount;
            }
        }
    }


    /**
     * This bank applies the transactions concurrently, in 8 threads. In order to get the right results, a lock must
     * be held while altering the balances. (This lock will be heavily contended though, hence the name of the class.)
     */
    static class ContendedBank extends BusyBank {
        static final int THREAD_COUNT = 8;

        @Override
        void runTransactions() throws InterruptedException {
            // TODO!
        }
    }


    /**
     * Like the ContendedBank, only with a separate lock for each balance.
     */
    static class ConcurrentBank extends BusyBank {

        static final int THREAD_COUNT = 8;

        @Override
        void runTransactions() throws InterruptedException {
            // TODO!
        }
    }
}
