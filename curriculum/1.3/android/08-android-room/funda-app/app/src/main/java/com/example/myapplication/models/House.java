package com.example.myapplication.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class House {
    @PrimaryKey(autoGenerate = true)
    public long id;

    public String streetName;
    public int houseNumber;

    public int askingPrice;
    public int size; // m2

    @NonNull
    @Override
    public String toString() {
        return streetName + " " + houseNumber + " (" + size +" m2, €" + askingPrice + ")";
    }
}
